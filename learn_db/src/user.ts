import { AppDataSource } from "./data-source"
import { Role } from "./entity/Role"
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {
    const usersRepoisutiry = AppDataSource.getRepository(User)
    const rolesRepository = AppDataSource.getRepository(Role)

    const adminRole = await rolesRepository.findOneBy({ id: 1 })
    const userRole = await rolesRepository.findOneBy({ id: 2 })

    await usersRepoisutiry.clear();
    console.log("Inserting a new user into the memory...")
    var user = new User()
    user.id = 1
    user.email = "admin@email.com"
    user.gender = "male"
    user.password = "Pass@1234"
    user.roles = [];
    user.roles.push(adminRole)
    user.roles.push(userRole)
    console.log("Inserting a new user into the database...")
    await usersRepoisutiry.save(user);


    user = new User()
    user.id = 2
    user.email = "user1@email.com"
    user.gender = "male"
    user.password = "Pass@1234"
    user.roles = [];
    user.roles.push(userRole)
    console.log("Inserting a new user into the database...")
    await usersRepoisutiry.save(user);


    user = new User()
    user.id = 3
    user.email = "user2@email.com"
    user.gender = "female"
    user.password = "Pass@1234"
    user.roles = [];
    user.roles.push(userRole)
    console.log("Inserting a new user into the database...")
    await usersRepoisutiry.save(user);

    const users = await usersRepoisutiry.find({ relations: { roles: true } })
    console.log(JSON.stringify(users, null, 2))

    const roles = await rolesRepository.find({ relations: { users: true } })
    console.log(JSON.stringify(roles, null, 2))


}).catch(error => console.log(error))
